#Tinyblog
Tinyblog is a simple blogging application for Flask, support for tags, posts, and comes with templates and basic CSS for ease of display.

##Requirements:
* python2.7

##Installation

In a virtualenvironment run the following in the project root folder (TinyBlog/) install either using pip:
```
$ pip install .
```
or using setuptools directly:

```
$ python setup.py install
```

If you are not using a virtual environment (not recommended), then the above commands will require "sudo"

Now, our interpreter can run our application using the following

```
$ tinyblog_run
```

This will start the web application using a simple server. Open your web browser and point it to http://localhost:5000/blog to see a simple, barebones sample of the blog (with an unwanted first entry!)

## Using the Module

The application can also be imported and run from an interpreter directly.

```python
import tinyblog
tinyblog.app.run(debug=True)
```

However, due to the nature of the development of this application this is not an elegant usage.

## What Could Be Better?
* Ideally our configuration would be more intuitive, having our config.py hiding deep down in the dregs of our installed packages is unintuitive. Ideally these would live at proper configuration location (depending on OS and style)
* The same goes for our templates, static files, and sqlite database - Django handles this correctly, letting the user *pull* out these files with "helper" scripts.
* Our application has many holes in it, the database models need to be seperated out from the blueprint, and the database configuration should be read correctly from our configuration file with the correct application context. Frankly, the models themselves need to be overhauled, since there is a lot of unnecessary words in the handling of tags. There should be an easy way to write blog entries as well!
* We should have a much more robust script to manage and run the web server (testing, database creation, etc)
* Our setup.py could have an option for development, which would include tests, and the packages to utilize those tests correctly.
