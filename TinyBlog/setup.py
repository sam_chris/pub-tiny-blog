#/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='TinyBlog',
    version='0.1',
    description='A small blogging application',
    url='https://bitbucket.org/sam_chris/pub-tiny-blog/',
    author='Samuel Christian',
    author_email='samchristian88@gmail.com',
    license='MIT',
    classifiers=[
        'Programming Language :: Python :: 2.7',
        'Private :: Do Not Upload',
    ],

    # Find the packages using init files automatically
    packages=find_packages(),

    # These are the packages required for installation/usage
    install_requires=['Flask>=0.10.1', 'flask-bcrypt', 'sqlalchemy', 'babel'],

    # Include script in the bin folder
    scripts=["bin/tinyblog_run"],

    # Extra data that needs to come along for the ride
    package_data={
        'tinyblog': ['static/css/*', 'templates/*', 'data/*']},
#        'tinyblog': ['templates/*'],
#        'tinyblog': ['data/*']},
)
