# filename: tiny_blog.py
# description: Models for the mini blog on sam-christian.com
# requirements: flask, sqlalchemy 
#               "config.py"

########################
# Built Ins
########################

from datetime import datetime
import os.path


########################
# External Imorts
########################

from flask.ext.bcrypt import Bcrypt

from sqlalchemy import create_engine, Text, Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref, sessionmaker, scoped_session

# These models should live outside the blueprint due to application context constraints.

engine = create_engine('sqlite:///{}/../data/tinyblog.sqlite'.format(os.path.dirname(os.path.abspath(__file__))))

session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))

Base = declarative_base()
Base.query = session.query_property

Base.metadata.create_all(engine)    

class Admin(Base):
    """
    Admin class for adding / editing entries, this functionality is unused
    """
    __tablename__ = 'admin'
    id = Column(Integer, primary_key=True)
    uname = Column(String(255))
    password = Column(String(60))
 
    def __init__(self, uname, password):
        self.name = uname
        self.password = password


class Entry(Base):
    """
    Entry class, dictates the blog entries
    """
    __tablename__ = 'blog_entries'
    id = Column(Integer, primary_key=True)
    content = Column(Text, nullable=False)
    title = Column(String(255), nullable=False, unique=True)
    date_added = Column(DateTime, nullable=True, default=datetime.now())
    tags = relationship('Tag', backref='blog_entries')

    def __init__(self, title, content, category, tags):
        """
        :param title: Title of the blog post
        :param content: Content as a string
        :param category: Broad category
        :param tags: List of tags
        """
        self.title = title
        self.content = content
        self.date_added = datetime.now()
        # Make db entries based on the blog entry tags as well!
        for tag in tags:
            self.add_tag(tag)

    def add_tag(self, tag_name):
        """
        Add a related tag to a blog entry
        :param tag_name: tag name. This should be an object instead...
        :return:
        """
        possible_tag = session.query(TagString).filter(TagString.tag_string == tag_name).first()
        if possible_tag:
            session.add(self)
            session.flush()
            the_tag = Tag(self.id, possible_tag.id)
            self.tags.append(the_tag)
            session.add(the_tag)
            session.add(self)
            session.commit()
        else:
            new_tag = TagString(tag_name)
            session.add(new_tag)
            session.add(self)
            session.flush()
            tag_rel = Tag(self.id, new_tag.id)
            self.tags.append(tag_rel)
            session.add(tag_rel)
            session.add(self)
            session.commit()
            

class Tag(Base):
    """
    Tag Class
    """
    __tablename__ = 'tag'
    id = Column(Integer, primary_key=True)
    entry_id = Column(Integer, ForeignKey('blog_entries.id'))
    entry = relationship("Entry", backref=backref('tag', order_by=id))
    tag_string = relationship("TagString", backref='tag')
    tag_string_id = Column(Integer, ForeignKey("raw_tag.id")) 

    def __init__(self, entry_id, tag_id):
        """
        Tag Class
        :param entry_id: tag name. This should be an object instead...
        :param tag_id: id of tag. :\
        """
        self.entry_id = entry_id
        self.tag_string_id = tag_id


class TagString(Base):
    """
    String of Tag
    """
    __tablename__ = 'raw_tag'
    id = Column(Integer, primary_key=True)
    tag_string = Column(String(255), unique=True)
    def __init__(self, tag_name):
        self.tag_string = tag_name    

