#! /usr/bin/python

from flask import Blueprint, render_template
from tinyblog.blog.models import *

blog = Blueprint("tinyblog", __name__, url_prefix="/blog")


#############################
# VIEWS
#############################

@blog.route('/')
def blog_page():
    blog_entries, tags = retrieve_blog_nec()
    recent_entries = session.query(Entry).order_by(Entry.date_added.desc()).limit(10).all()
    return render_template('blog_template.html', tags=tags, blog_entries = blog_entries, preview_entries = recent_entries)

@blog.route('/entry/<blog_id>')
def blog_entry(blog_id):
    blog_entries, tags = retrieve_blog_nec()
    entry = session.query(Entry).filter(Entry.id == blog_id).first()
    return render_template('blog.html', tags = tags, blog_entries = blog_entries, entry = entry)

@blog.route('/tag/<tag_name>')
def get_tagged(tag_name):
    blog_entries, tags = retrieve_blog_nec()
    tagged = session.query(TagString).filter(TagString.tag_string == tag_name).first()
    tagged_entries = []
    for entry in tagged.tag:
        tagged_entries.append(entry.entry)
    tagged_entries = sorted(tagged_entries, key = lambda x: x.date_added, reverse = True)
    return render_template('blog_template.html', tags = tags, blog_entries = blog_entries, preview_entries = tagged_entries)

###################################
# FUNCTION
###################################
def retrieve_blog_nec():
    blog_entries = session.query(Entry.date_added, Entry.title, Entry.id).all()
    tags = session.query(TagString.tag_string).all()
    return blog_entries, tags
