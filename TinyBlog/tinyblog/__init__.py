from flask import Flask
from tinyblog.filters import format_dt

app = Flask(__name__)
app.config.from_object("tinyblog.config")

from tinyblog.blog.views import blog

app.register_blueprint(blog)

app.jinja_env.filters['datetime'] = format_dt
